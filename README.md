# Bebras-automation

Scriptini per semplificare la vita a chi lavora nell'helpdesk di Bebras.

## backend-notify.py

Invia un messaggio su Telegram quando dei nuovi esami problematici compaiono nel backend.

### Esecuzione

Inanzitutto, rinomina e modifica il file `.env.example` in `.env`.

	$ docker build -t bebras .
	$ docker run --name bebras --env-file .env bebras

