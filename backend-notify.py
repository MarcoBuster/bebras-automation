#!/usr/bin/python3

import requests
from requests.auth import HTTPBasicAuth
from bs4 import BeautifulSoup
from dataclasses import dataclass
import sqlite3
import time
from datetime import datetime
import botogram

import os

TABLES = [
    "Error",
    "NeedCheck",
    "MultiExams",
    "RunningOut",
    "RunningOutBug",
    "DateOut",
]

conn = sqlite3.connect("cache.db")
c = conn.cursor()
c.execute("CREATE TABLE IF NOT EXISTS exams(exam_id PRIMARY KEY)")
conn.commit()

bot = botogram.create(os.environ.get("TELEGRAM_BOT_TOKEN"))
chat = bot.chat(os.environ.get("TELEGRAM_CHAT_ID"))


@dataclass
class Exam:
    exam_id: int
    login_id: int
    team_name: str
    teacher_last_name: str
    points: int
    time: str
    notes: str

    def __str__(self):
        return "\t".join(
            map(
                lambda e: str(e),
                [
                    self.exam_id,
                    self.login_id,
                    self.team_name,
                    self.teacher_last_name,
                    self.points,
                    self.time,
                    self.notes,
                ],
            )
        )


def parse_page():
    url = f"https://bebras.it/backend/?edition={os.environ.get('EDITION')}&act=overview"
    auth = HTTPBasicAuth(os.environ.get("USERNAME"), os.environ.get("PASSWORD"))
    response = requests.get(url, auth=auth)
    soup = BeautifulSoup(response.text, "html.parser")

    tables = soup.find_all("table", {"class": "data"})

    parsed = {}

    # Parse table
    for i, table in enumerate(tables):
        rows = table.find_all("tr")

        # Parse row
        exams = []
        for row in rows[1:]:
            columns = row.find_all("td")
            exams.append(
                Exam(
                    exam_id=int(columns[0].text),
                    login_id=int(columns[3].text),
                    team_name=columns[5].text,
                    teacher_last_name=columns[8].text,
                    points=int(columns[12].text),
                    time=columns[13].text,
                    notes=columns[14].text,
                )
            )
        parsed[TABLES[i]] = exams
    return parsed


def maybe_notify_user(exam, kind):
    c.execute("SELECT 1 FROM exams WHERE exam_id=?", (exam.exam_id,))
    if c.fetchone():
        return

    chat.send(
        f"<b>#{kind}</b>"
        f"\n<b>Exam ID</b>: {exam.exam_id}"
        f"\n<b>Login ID</b>: {exam.login_id}"
        f"\n<b>Team name</b>: {exam.team_name}"
        f"\n<b>Teacher name</b>: {exam.teacher_last_name}"
        f"\n<b>Points</b>: {exam.points}"
        f"\n<b>Time</b>: {exam.time}"
        f"\n<b>Notes</b>: {exam.notes}",
        syntax="html",
    )

    print(datetime.now(), kind, exam, sep="\t")

    c.execute("INSERT INTO exams VALUES(?)", (exam.exam_id,))
    conn.commit()


def main():
    while True:
        try:
            parsed = parse_page()
        except Exception as e:
            print("Exception", e)
            continue

        for issue_kind in parsed:
            issues = parsed[issue_kind]

            for issue in issues:
                maybe_notify_user(issue, issue_kind)
        time.sleep(int(os.environ.get("POLLING_RATE", 300)))


if __name__ == "__main__":
    main()
